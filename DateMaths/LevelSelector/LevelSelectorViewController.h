//
//  LevelSelectorViewController.h
//  DateMaths
//
//  Created by James Dunwoody on 28/06/2015.
//
//

#import <UIKit/UIKit.h>

@protocol LevelSelectorViewControllerDelegate

- (void)didChangeLevel;
@end

@class LevelSelectorDataSource;
@class DateMathsViewController;
@class LevelCollection;

@interface LevelSelectorViewController : UIViewController<UICollectionViewDelegate>
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@property (nonatomic) LevelSelectorDataSource *dataSource;
@property (nonatomic) id<LevelSelectorViewControllerDelegate> delegate;

@property (nonatomic, strong) LevelCollection *levelCollection;
- (void)reloadData;
@property (weak, nonatomic) IBOutlet UIVisualEffectView *visualEffectsView;

@end
