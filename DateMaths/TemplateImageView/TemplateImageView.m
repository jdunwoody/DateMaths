#import "TemplateImageView.h"

@implementation TemplateImageView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setup];
    }

    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self setup];
}

- (void)setup
{
    self.image = [self.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
}

- (void)prepareForInterfaceBuilder
{
    // IB_DESIGNABLE bug workaround.
    // For some reason doing this here works
    self.image = [self.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
}

#pragma mark - UIImageView

- (void)setImage:(UIImage *)image
{
    if (image.renderingMode != UIImageRenderingModeAlwaysTemplate) {
        image = [image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    }

    [super setImage:image];
}

@end
