//
//  Scoring.m
//  DateMaths
//
//  Created by James Dunwoody on 5/07/2015.
//
//

#import "Scoring.h"
#import "LevelItem.h"
#import "ResultsCollection.h"
#import "DigitCollection.h"

@interface Scoring ()
@property (nonatomic, readonly) NSUInteger maxPoints;
@property (nonatomic, readonly) NSUInteger regularPoints;
@property (nonatomic, readonly) NSUInteger minPoints;
@end

@implementation Scoring

- (instancetype)init
{
    self = [super init];

    if (self) {
        _maxPoints = 100;
        _regularPoints = 50;
        _minPoints = 10;
    }

    return self;
}

- (NSUInteger)calculate:(LevelItem *)levelItem
{
    NSUInteger score = 0;

    if (levelItem.resultsCollection.sum.unsignedIntegerValue == levelItem.number) {
        int numDigitsUsed = levelItem.digitCollection.numUsed;
        NSInteger totalDigits = levelItem.digitCollection.count;

        if (numDigitsUsed == totalDigits) {
            score = self.maxPoints;

        } else if (numDigitsUsed > (NSUInteger)(totalDigits / 2.0f)) {
            score = self.regularPoints;

        } else {
            score = self.minPoints;
        }
    }

    return score;
}

@end
