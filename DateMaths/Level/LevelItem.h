//
// Created by James Dunwoody on 6/04/15.
// Copyright (c) 2015 ___FULLUSERNAME___. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DataItem.h"

@class DigitCollection;
@class OperatorCollection;
@class ResultsCollection;
@class Digit;
@class Operator;
@class OperatorFactory;
@class DigitFactory;

@interface LevelItem : NSObject<DataItem>

@property (nonatomic, readonly) NSInteger number;
@property (nonatomic) NSString *value;
@property (nonatomic) NSString *displayValue;

@property (nonatomic) BOOL dragging;

- (instancetype)init __unavailable;
- (instancetype)initWithNumber:(NSInteger)number digitFactory:(DigitFactory *)digitFactory operatorFactory:(OperatorFactory *)operatorFactory;

@property (nonatomic) BOOL starA;
@property (nonatomic) BOOL starB;
@property (nonatomic) BOOL starC;

@property (nonatomic, readonly) DigitCollection *digitCollection;
@property (nonatomic, readonly) OperatorCollection *operatorCollection;
@property (nonatomic, readonly) ResultsCollection *resultsCollection;

@property (nonatomic, readonly) BOOL attempted;
@property (nonatomic, readonly) NSUInteger points;
- (void)updateStarsWithSum:(NSNumber *)sum witDigitsCollection:(DigitCollection *)digitCollection;

- (void)useDigit:(Digit *)digit;
- (void)useOperator:(Operator *)anOperator;
@end
