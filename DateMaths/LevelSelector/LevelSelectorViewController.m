//
//  LevelSelectorViewController.m
//  DateMaths
//
//  Created by James Dunwoody on 28/06/2015.
//
//

#import "LevelSelectorViewController.h"
#import "LevelSelectorDataSource.h"
#import "LevelCollection.h"

@interface LevelSelectorViewController ()
@property (weak, nonatomic) IBOutlet UILabel *totalLabel;

@end

@implementation LevelSelectorViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.collectionView.dataSource = self.dataSource;
    self.collectionView.delegate = self;

    UINib *nib = [UINib nibWithNibName:@"LevelSelectorCollectionViewCell" bundle:nil];

    [self.collectionView registerNib:nib forCellWithReuseIdentifier:@"levelSelectorCell"];
    self.totalLabel.text = [NSString stringWithFormat:@"%lu pts", (unsigned long)self.levelCollection.score];
}

- (void)reloadData
{
    [self.collectionView reloadData];
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [self.levelCollection updateCurrentWithRow:(NSUInteger)indexPath.row];
    [self.delegate didChangeLevel];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)clickedLevelCollectionAtIndexpath:(NSIndexPath *)indexPath
{
    [self changeCurrentLevelTo:indexPath.row];
}

- (void)changeCurrentLevelTo:(NSInteger)row
{
}

@end
