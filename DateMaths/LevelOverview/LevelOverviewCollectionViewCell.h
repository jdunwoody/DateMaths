//
//  LevelOverviewCollectionViewCell.h
//  DateMaths
//
//  Created by James Dunwoody on 28/06/2015.
//
//

#import <UIKit/UIKit.h>

@interface LevelOverviewCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *label;

- (void)configure;
- (void)configureAsSelected:(BOOL)selected;
@end
