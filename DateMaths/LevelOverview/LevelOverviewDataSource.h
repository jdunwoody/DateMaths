//
// Created by James Dunwoody on 28/06/15.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class LevelCollection;

@interface LevelOverviewDataSource : NSObject<UICollectionViewDataSource>

- (instancetype)init __unavailable;
- (id)initWithLevelCollection:(LevelCollection *)levelCollection;
@end
