//
// Created by James Dunwoody on 15/06/15.
//

#import "TweakableProperty.h"
#import "IntroViewController.h"


@interface TweakableProperty ()
@end

@implementation TweakableProperty


- (instancetype)initWithLabel:(NSString *)label targetSelector:(SEL)stepperValueChangedSelector target:(IntroViewController *)stepperValueChangedTarget
{
    self = [super init];
    if (self) {
        _label = label;
        _stepperValueChangedSelector = stepperValueChangedSelector;
        _stepperValueChangedTarget = stepperValueChangedTarget;
    }
    return self;
}

@end
