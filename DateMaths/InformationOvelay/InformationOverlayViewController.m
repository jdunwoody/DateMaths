//
//  InformationOverlayViewController.m
//  DateMaths
//
//  Created by James Dunwoody on 13/07/2015.
//
//

#import "InformationOverlayViewController.h"
#import "CALayer+DateMathsGlow.h"
#import "Theme.h"

@interface InformationOverlayViewController ()
@property (weak, nonatomic) IBOutlet UILabel *infoLabel;

@end

@implementation InformationOverlayViewController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    [self.infoLabel.layer dateMaths_glowWithColor:[Theme mainColour]];
    self.infoLabel.textColor = [Theme mainColour];
}

- (void)showUpdatedScore:(NSUInteger)score animate:(BOOL)animate
{
    self.infoLabel.text = [NSString stringWithFormat:@"%lu", (unsigned long)score];

    if (animate) {
        self.view.superview.hidden = NO;

        self.infoLabel.font = [UIFont fontWithDescriptor:self.infoLabel.font.fontDescriptor size:70.0f];

        self.infoLabel.center = self.view.superview.center;
        [self.infoLabel setNeedsLayout];
        self.infoLabel.transform = CGAffineTransformScale(self.infoLabel.transform, 0.5f, 0.5f);

        [UIView animateKeyframesWithDuration:1.0f delay:0.0f options:UIViewKeyframeAnimationOptionCalculationModeLinear animations:^(void) {
            self.infoLabel.alpha = 1.0f;

            self.infoLabel.center = CGPointMake(self.infoLabel.center.x, 100.0f);
            self.infoLabel.transform = CGAffineTransformScale(self.infoLabel.transform, 2.0f, 2.0f);

        } completion:^(BOOL finished) {
            [UIView animateKeyframesWithDuration:0.5f delay:0.0f options:UIViewKeyframeAnimationOptionCalculationModeLinear animations:^(void) {
                self.infoLabel.alpha = 0.0f;
                self.infoLabel.center = CGPointMake(self.infoLabel.center.x, 0.0f);

            } completion:^(BOOL finished2) {
                self.view.superview.hidden = YES;
            }];
        }];
    }
}

- (void)showInformation:(NSString *)text animated:(BOOL)animated
{
    if (animated) {
        self.view.superview.hidden = NO;
        self.infoLabel.text = text;
        self.infoLabel.transform = CGAffineTransformScale(self.infoLabel.transform, 0.25f, 0.25f);

        [UIView animateKeyframesWithDuration:0.5f delay:0.0f options:UIViewKeyframeAnimationOptionCalculationModeLinear animations:^(void) {
            self.infoLabel.alpha = 1.0f;
            self.infoLabel.transform = CGAffineTransformScale(self.infoLabel.transform, 4.0f, 4.0f);

        } completion:^(BOOL finished) {
            [UIView animateKeyframesWithDuration:0.5f delay:0.0f options:UIViewKeyframeAnimationOptionCalculationModeLinear animations:^(void) {
                self.infoLabel.alpha = 0.0f;

            } completion:^(BOOL finished2) {
                self.view.superview.hidden = YES;
            }];
        }];
    }
}

@end
