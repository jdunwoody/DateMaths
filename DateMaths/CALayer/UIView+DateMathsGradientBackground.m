//
// Created by James Dunwoody on 6/06/15.
//

#import "UIView+DateMathsGradientBackground.h"
#import "Theme.h"


@implementation UIView (DateMathsGradientBackground)

- (void)dateMaths_gradientBackgroundWhiteToGray
{
    UIColor *topColour = [Theme backgroundGradientTopColour];
    UIColor *bottomColour = [Theme backgroundGradientBottomColour];

    [self dateMaths_gradientFromColour:topColour toColour:bottomColour];
}

- (void)dateMaths_gradientFromColour:(UIColor *)color toColour:(UIColor *)colour
{
    CAGradientLayer *gradientLayer = [CAGradientLayer layer];

    UIColor *topColour = [Theme backgroundGradientTopColour];
    UIColor *bottomColour = [Theme backgroundGradientBottomColour];

    gradientLayer.colors = @[
        (id)topColour.CGColor,
        (id)bottomColour.CGColor,
    ];
    gradientLayer.locations = @[@0, @1];

    CALayer *gradientColorLayer = gradientLayer;
    gradientColorLayer.frame = self.bounds;
    [self.layer insertSublayer:gradientColorLayer atIndex:0];
}

- (void)dateMaths_gradientRainbowBackground
{
    CAGradientLayer *gradientLayer = [CAGradientLayer layer];

    NSMutableArray *colourRefs = [NSMutableArray array];
    for (UIColor *colour in [Theme rainbowColours]) {
        [colourRefs addObject:(id)colour.CGColor];
    }
    gradientLayer.colors = colourRefs;

    NSUInteger numberOfColours = gradientLayer.colors.count;
    NSMutableArray *locationArray = [NSMutableArray arrayWithCapacity:numberOfColours];
    for (NSUInteger i = 0; i < numberOfColours - 1; i++) {
        [locationArray addObject:@(((CGFloat)i) / numberOfColours)];
    }
    [locationArray addObject:@(1.0f)];

    gradientLayer.locations = locationArray;
    gradientLayer.frame = self.bounds;

    [self.layer insertSublayer:gradientLayer atIndex:0];
}

@end
