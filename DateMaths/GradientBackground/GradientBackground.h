//
//  GradientBackground.h
//  DateMaths
//
//  Created by James Dunwoody on 14/06/2015.
//
//

#import <UIKit/UIKit.h>

IB_DESIGNABLE
@interface GradientBackground : UIView

@property (nonatomic) IBInspectable UIColor *fromColour;
@property (nonatomic) IBInspectable UIColor *toColour;

@end
