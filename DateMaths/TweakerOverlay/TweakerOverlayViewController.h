//
//  TweakerOverlayViewController.h
//  DateMaths
//
//  Created by James Dunwoody on 15/06/2015.
//
//

#import <UIKit/UIKit.h>
#import "TweakerTableCellTableViewCell.h"

@interface TweakerOverlayViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>

@property (nonatomic) NSArray *tweakableProperties;
@property (nonatomic, strong) NSArray *properties;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@end
