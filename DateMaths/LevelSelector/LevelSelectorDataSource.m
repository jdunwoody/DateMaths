//
// Created by James Dunwoody on 28/06/15.
//

#import "LevelSelectorDataSource.h"
#import "LevelCollection.h"
#import "LevelSelectorCollectionViewCell.h"
#import "LevelItem.h"

@interface LevelSelectorDataSource ()
@property (nonatomic, readonly) LevelCollection *levelCollection;
@end

@implementation LevelSelectorDataSource

- (instancetype)initWithLevelCollection:(LevelCollection *)levelCollection
{
    self = [super init];

    if (self) {
        _levelCollection = levelCollection;
    }

    return self;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.levelCollection.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    LevelSelectorCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"levelSelectorCell" forIndexPath:indexPath];

    cell.label.text = self.levelCollection[(NSUInteger)indexPath.row];//currentData.stringValue;
    LevelItem *levelItem = self.levelCollection[(NSUInteger)indexPath.row];
    [cell configureWithLevelItem:levelItem];

    return cell;
}

@end
