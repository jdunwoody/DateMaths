#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface CALayer (DateMathsGlow)

- (void)dateMaths_glowWithColor:(UIColor *)color;
- (void)dateMaths_borderWithColor:(UIColor *)color;

- (void)dateMaths_smallBorderWithColor:(UIColor *)color;
- (void)dateMaths_smallGlowWithColor:(UIColor *)color;
- (void)dateMaths_scaledBorderWithColor:(UIColor *)color;
- (void)dateMaths_borderWithColor:(CGColorRef)color radius: (CGFloat)radius borderWidth:(CGFloat)borderWidth;
@end
