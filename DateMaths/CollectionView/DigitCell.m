//
//  DigitCell.m
//  DateMaths
//
//  Created by James Dunwoody on 8/06/2015.
//
//

#import "DigitCell.h"
#import "Theme.h"
#import "CALayer+DateMathsGlow.h"
#import "UIView+DateMaths.h"

@interface DigitCell ()
@end

@implementation DigitCell

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];

    if (self) {
        [self setupViewFromNib];

        [self configureDefaults];
        [self configure:DigitCellMedium];
    }

    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];

    if (self) {
        [self setupViewFromNib];

        [self configureDefaults];
        [self configure:DigitCellMedium];
    }

    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];

    [self configureDefaults];
    [self configure:DigitCellMedium];
}

- (void)configureDefaults
{
    self.starA.hidden = YES;
    self.starB.hidden = YES;
    self.starC.hidden = YES;
}

- (void)prepareForInterfaceBuilder
{
    [super prepareForInterfaceBuilder];
    [self configure:DigitCellMedium];
}

- (void)configure:(DigitCellCellSize)cellSize
{
    [self.layer dateMaths_glowWithColor:[Theme shadowColour]];
    if (cellSize == DigitCellMedium) {
        [self.layer dateMaths_borderWithColor:self.colour];
    } else {
        [self.layer dateMaths_borderWithColor:self.colour.CGColor radius:4.0f borderWidth:2.0f];
    }

    self.label.textColor = self.colour;
    self.label.text = self.text;
}

- (void)unilluminate
{
    self.label.textColor = [Theme deselectedColour];
    self.layer.borderColor = [Theme deselectedColour].CGColor;
}

@end
