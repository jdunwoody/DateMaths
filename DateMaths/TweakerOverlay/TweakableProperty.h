//
// Created by James Dunwoody on 15/06/15.
//

#import <Foundation/Foundation.h>

@class IntroViewController;


@interface TweakableProperty : NSObject

@property (nonatomic, readonly) NSString *label;
@property (nonatomic, readonly) SEL stepperValueChangedSelector;
@property (nonatomic, readonly) IntroViewController *stepperValueChangedTarget;

- (instancetype)init __unavailable;
- (instancetype)initWithLabel:(NSString *)label targetSelector:(SEL)stepperValueChangedSelector target:(IntroViewController *)stepperValueChangedTarget;

@end
