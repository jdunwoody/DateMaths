#import "DateMathsViewController.h"
#import "OperatorCollection.h"
#import "DigitCollectionDataSource.h"
#import "OperatorCollectionDataSource.h"
#import "ResultsCollectionViewDataSource.h"
#import "Digit.h"
#import "LevelCollectionViewDataSource.h"
#import "LevelCollection.h"
#import "ResultsCollection.h"
#import "LevelItem.h"
#import "Operator.h"
#import "OperatorFactory.h"
#import "DigitFactory.h"
#import "DigitCollection.h"
#import "ResultCollectionViewLayout.h"
#import "SimpleCollectionViewCell.h"
#import "DataItemView.h"
#import "UIViewController+navigation.h"
#import "Theme.h"
#import "Sounds.h"
#import "UIView+DateMathsGradientBackground.h"
#import "LevelOverviewDataSource.h"
#import "LevelSelectorDataSource.h"
#import "InformationOverlayViewController.h"

@interface DateMathsViewController ()

@property (nonatomic, readonly) NSString *formattedDate;

@property (nonatomic, strong) DigitCollectionDataSource *digitCollectionDataSource;
@property (nonatomic, strong) OperatorCollectionDataSource *operatorCollectionDataSource;
@property (nonatomic, strong) ResultsCollectionViewDataSource *resultsCollectionViewDataSource;
@property (nonatomic, strong) LevelCollectionViewDataSource *levelCollectionViewDataSource;

@property (nonatomic, strong) LevelCollection *levelCollection;
@property (nonatomic, strong) UIView *draggingImageView;
@property (nonatomic, strong) ResultCollectionViewLayout *layout;
@property (nonatomic, strong) id<DataItem> selectedDataItem;
@property (nonatomic, strong) Sounds *sounds;
@property (nonatomic, readonly) LevelOverviewViewController *levelOverviewViewController;
//@property (nonatomic, readonly) LevelSelectorDataSource *levelSelectorDataSource;
@property (nonatomic, readonly) InformationOverlayViewController *informationOverlayViewController;
@property (nonatomic) float previousScore;
@end

@implementation DateMathsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    if (!self.date) {
        self.date = [NSDate date];
    }

    NSAssert(self.date, @"Didn't set a self.date");

    UINib *nib = [UINib nibWithNibName:@"SimpleCollectionViewCell" bundle:[NSBundle mainBundle]];

    DigitFactory *digitFactory = [[DigitFactory alloc] initWithDate:self.date];
    OperatorFactory *operatorFactory = [[OperatorFactory alloc] initWithSymbols:@[@"(", @")", @"*", @"/", @"+", @"-"]];
    self.levelCollection = [[LevelCollection alloc] initWithDigitFactory:digitFactory operatorFactory:operatorFactory];

    //Level Overview
    _levelOverviewViewController = [[LevelOverviewViewController alloc] initWithNibName:@"LevelOverviewViewController" bundle:nil];
    self.levelOverviewViewController.delegate = self;
    LevelOverviewDataSource *levelOverviewDataSource = [[LevelOverviewDataSource alloc] initWithLevelCollection:self.levelCollection];
    self.levelOverviewViewController.dataSource = levelOverviewDataSource;

    [self addChildViewController:self.levelOverviewViewController];
    [self.levelOverviewContainer addSubview:self.levelOverviewViewController.view];
    [self.levelOverviewViewController didMoveToParentViewController:self];

    //Level
    self.levelCollectionViewDataSource = [[LevelCollectionViewDataSource alloc] initWithCollection:self.levelCollection];

    //Digits
    self.digitCollectionDataSource = [[DigitCollectionDataSource alloc] initWithLevelCollection:self.levelCollection withDelegate:self];
    self.digitCollectionView.dataSource = self.digitCollectionDataSource;
    self.digitCollectionView.delegate = self;
    [self.digitCollectionView registerNib:nib forCellWithReuseIdentifier:@"simpleCell"];

    //Operators
    self.operatorCollectionDataSource = [[OperatorCollectionDataSource alloc] initWithLevelCollection:self.levelCollection];
    self.operatorCollectionView.dataSource = self.operatorCollectionDataSource;
    self.operatorCollectionView.delegate = self;
    [self.operatorCollectionView registerNib:nib forCellWithReuseIdentifier:@"simpleCell"];

    //Results
    self.resultsCollectionViewDataSource = [[ResultsCollectionViewDataSource alloc] initWithLevelCollection:self.levelCollection];
    self.resultsCollectionView.dataSource = self.resultsCollectionViewDataSource;
    self.resultsCollectionView.delegate = self;
    [self.resultsCollectionView registerNib:nib forCellWithReuseIdentifier:@"simpleCell"];

    self.layout = (ResultCollectionViewLayout *)self.resultsCollectionView.collectionViewLayout;
    self.layout.levelCollection = self.levelCollection;

    self.totalLabel.text = [self formatSum:@0];
    self.totalLabel.textColor = [Theme mainColour];
    self.sounds = [[Sounds alloc] init];
    [self playBackgroundMusic];

    [self dateMaths_showNavigationController];

    [self.backgroundView dateMaths_gradientBackgroundWhiteToGray];

    [self didReceiveNewTarget:self.levelCollection.current.number];

    _informationOverlayViewController = [[InformationOverlayViewController alloc] initWithNibName:@"InformationOverlayViewController" bundle:nil];
    [self addChildViewController:self.informationOverlayViewController];
    self.informationOverlayViewController.view.frame = self.scoreContainer.bounds;
    [self.scoreContainer addSubview:self.informationOverlayViewController.view];
    [self.informationOverlayViewController didMoveToParentViewController:self];

    self.navigationController.navigationBar.hidden = NO;

    self.previousScore = 0.0f;
    [self updateTitle:0];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    for (CALayer *layer in self.backgroundView.layer.sublayers) {
        layer.frame = self.view.bounds;
    }

    CGRect rect = self.levelOverviewContainer.bounds;
    self.levelOverviewViewController.view.frame = rect;
//    self.levelOverviewViewController.view.frame = CGRectMake(0.0f, 0.0f, 600.0f, 120.0f);

//    self.levelOverviewViewController.view.frame = self.levelOverviewContainer.bounds;

    [self updateGame];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];

    NSString *currentLevelMessage = [NSString stringWithFormat:@"%ld", (long)self.levelCollection.current.number];
    [self.informationOverlayViewController showInformation:currentLevelMessage animated:YES];
}

- (void)didReceiveNewTarget:(double)newTarget
{
}

- (void)playBackgroundMusic
{
    [self.sounds playBackgroundMusic];
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (collectionView == self.digitCollectionView) {
        [self clickedDigitCollectionAtIndexPath:indexPath];

    } else if (collectionView == self.operatorCollectionView) {
        [self clickedOperatorCollectionAtIndexPath:indexPath];

    } else if (collectionView == self.resultsCollectionView) {
        [self clickedResultsCollectionAtIndexPath:indexPath];

    }

    [self updateGame];
}

- (void)updateGame
{
    [self updateSum];

    NSUInteger score = self.levelCollection.score;
    [self updateScoreWithScore:score];
    [self showStar];
}

- (void)showStar
{

}

- (void)updateSum
{
    NSNumber *sum = self.levelCollection.current.resultsCollection.sum;
    self.totalLabel.text = [self formatSum:sum];
}

- (void)updateScoreWithScore:(NSUInteger)score
{
    if (score > self.previousScore) {
        [self updateTitle:score];

        [self.informationOverlayViewController showUpdatedScore:score animate:YES];
        self.previousScore = score;
    }
}

- (void)updateTitle:(NSUInteger)points
{
    self.title = [NSString stringWithFormat:@"%lu pts", (unsigned long)points];
}

- (void)clickedResultsCollectionAtIndexPath:(NSIndexPath *)indexPath
{
    id<DataItem> resultItem = self.resultsCollectionViewDataSource[indexPath.row];

    if ([resultItem isKindOfClass:[Digit class]]) {
        Digit *digit = resultItem;
        digit.used = NO;
        [self.digitCollectionView reloadData];
    }

    [self.resultsCollectionViewDataSource removeItem:resultItem];
    [self.resultsCollectionView reloadData];

    if ([resultItem isKindOfClass:Operator.class]) {
        [self.operatorCollectionView reloadData];
    } else {
        [self.digitCollectionView reloadData];
    }
}

- (void)clickedOperatorCollectionAtIndexPath:(NSIndexPath *)indexPath
{
    Operator *operatorTemplate = self.levelCollection.current.operatorCollection[(NSUInteger)indexPath.row];
    Operator *operator = [[Operator alloc] initWithOperator:operatorTemplate];

    [self.levelCollection.current useOperator:operator];

    [self.resultsCollectionView reloadData];
    [self.operatorCollectionView reloadItemsAtIndexPaths:@[indexPath]];
}

- (void)clickedDigitCollectionAtIndexPath:(NSIndexPath *)indexPath
{
    Digit *digit = self.levelCollection.current.digitCollection[(NSUInteger)indexPath.row];
    if (digit.used) {
        return;
    }

    [self.levelCollection.current useDigit:digit];

    digit.used = YES;

    [self.resultsCollectionView reloadData];
    [self.digitCollectionView reloadItemsAtIndexPaths:@[indexPath]];
}

- (void)didLayoutCell:(NSIndexPath *)path inCollectionView:(UICollectionView *)view
{
}

- (NSString *)formatSum:(NSNumber *)sum
{
    if (!sum) {
        return @"";
    }
    return [NSString stringWithFormat:@"= %i", [sum intValue]];
}

- (IBAction)panned:(id)sender
{
    if (self.dragResultsPanGestureRecogniser.state == UIGestureRecognizerStateBegan) {
        CGPoint location = [self.dragResultsPanGestureRecogniser locationInView:self.resultsCollectionView];
        NSIndexPath *indexPathForSelectedItem = [self.resultsCollectionView indexPathForItemAtPoint:location];
        if (indexPathForSelectedItem == nil) {
            return;
        }
        self.selectedDataItem = self.resultsCollectionViewDataSource[indexPathForSelectedItem.row];
        self.selectedDataItem.dragging = YES;
        [self.resultsCollectionView reloadItemsAtIndexPaths:@[indexPathForSelectedItem]];

        DataItemView *dataItemView = [self.layout dataitemViewNearestLocation:location];

        if (!self.draggingImageView) {
            self.draggingImageView = [[UIView alloc] initWithFrame:CGRectMake(dataItemView.rect.origin.x, dataItemView.rect.origin.y, 3, SIMPLE_COLLECTION_VIEW_CELL_HEIGHT)];
            [self.resultsCollectionView addSubview:self.draggingImageView];
        }

        self.draggingImageView.frame = CGRectMake(
            dataItemView.rect.origin.x,
            dataItemView.rect.origin.y,
            self.draggingImageView.frame.size.width,
            self.draggingImageView.frame.size.height);


    } else if (self.dragResultsPanGestureRecogniser.state == UIGestureRecognizerStateEnded) {
        [self.draggingImageView removeFromSuperview];
        self.draggingImageView = nil;

        CGPoint location = [self.dragResultsPanGestureRecogniser locationInView:self.resultsCollectionView];
        NSIndexPath *indexPathForTargetDataItem = [self.resultsCollectionView indexPathForItemAtPoint:location];
        if (indexPathForTargetDataItem == nil) {
            return;
        }
        id<DataItem> targetDataItem = self.resultsCollectionViewDataSource[indexPathForTargetDataItem.row];

        if (targetDataItem != self.selectedDataItem) {
            [self.levelCollection.current.resultsCollection moveDataItem:self.selectedDataItem toDataItem:targetDataItem];
        }

        self.selectedDataItem.dragging = NO;
        self.selectedDataItem = nil;
        for (id<DataItem> dataItem in self.levelCollection.current.resultsCollection) {
            if (dataItem.dragging) {
                NSAssert(NO, @"Shouldn't get here");
            }
        }
        [self.resultsCollectionView reloadData];
        [self updateGame];

    } else if (self.dragResultsPanGestureRecogniser.state == UIGestureRecognizerStateChanged) {
        CGPoint location = [self.dragResultsPanGestureRecogniser locationInView:self.resultsCollectionView];
        DataItemView *dataItemView = [self.layout dataitemViewNearestLocation:location];

        if (!CGPointEqualToPoint(self.draggingImageView.frame.origin, dataItemView.rect.origin)) {
            self.draggingImageView.frame = CGRectMake(
                dataItemView.rect.origin.x,
                dataItemView.rect.origin.y,
                self.draggingImageView.frame.size.width,
                self.draggingImageView.frame.size.height);

        }

    } else if (self.dragResultsPanGestureRecogniser.state == UIGestureRecognizerStateCancelled) {
        NSAssert(NO, @"Shouldn't get here");

    } else if (self.dragResultsPanGestureRecogniser.state == UIGestureRecognizerStateFailed) {
        NSAssert(NO, @"Shouldn't get here");

    } else {
        NSAssert(NO, @"Shouldn't get here");
    }
}

- (IBAction)backNavigationButtonTapped:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)resultTargetTapped
{
}

- (void)levelOverviewTapped
{
    [self showLevelSelector];
}

- (void)showLevelSelector
{
    [self performSegueWithIdentifier:@"levelSelector" sender:self];
}

- (void)changeCurrentLevelTo:(NSInteger)row
{
    LevelItem *target = self.levelCollection[(NSUInteger)row];
    self.levelCollection.current = target;

    [self didReceiveNewTarget:target.number];
    [self.resultsCollectionView reloadData];
    [self.operatorCollectionView reloadData];
    [self.digitCollectionView reloadData];
    [self.levelOverviewViewController reloadData];
}

- (IBAction)selectedStarButtonItem:(id)sender
{
    [self showLevelSelector];
}

- (IBAction)selectedCalendarButtonItem:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    LevelSelectorViewController *destinationViewController = segue.destinationViewController;
    LevelSelectorDataSource *levelSelectorDataSource = [[LevelSelectorDataSource alloc] initWithLevelCollection:self.levelCollection];

    destinationViewController.dataSource = levelSelectorDataSource;
    destinationViewController.delegate = self;
    destinationViewController.levelCollection = self.levelCollection;
}

- (void)didSelectLevel:(NSInteger)row
{
    LevelItem *target = self.levelCollection[(NSUInteger)row];
    self.levelCollection.current = target;
}

- (void)reloadEverything
{
    [self.resultsCollectionView reloadData];
    [self.operatorCollectionView reloadData];
    [self.digitCollectionView reloadData];
    [self.levelOverviewViewController reloadData];

    [self updateGame];
}

- (void)didChangeLevel
{
    [self reloadEverything];
}

@end
