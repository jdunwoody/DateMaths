//
// Created by James Dunwoody on 20/06/15.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class DigitCell;

IB_DESIGNABLE
@interface TitleView : UIView

@property (weak, nonatomic) IBOutlet DigitCell *dateDCell;
@property (weak, nonatomic) IBOutlet DigitCell *dateACell;
@property (weak, nonatomic) IBOutlet DigitCell *dateTCell;
@property (weak, nonatomic) IBOutlet DigitCell *dateECell;

@property (weak, nonatomic) IBOutlet DigitCell *mathsMCell;
@property (weak, nonatomic) IBOutlet DigitCell *mathsACell;
@property (weak, nonatomic) IBOutlet DigitCell *mathsTCell;
@property (weak, nonatomic) IBOutlet DigitCell *mathsHCell;
@property (weak, nonatomic) IBOutlet DigitCell *mathsSCell;

@end
