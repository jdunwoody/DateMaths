#import "SimpleCollectionViewCell.h"
#import "DigitCell.h"

@implementation SimpleCollectionViewCell

- (void)configure
{
    NSAssert(self.digitCell, @"No digit cell");

    [self.digitCell configure:DigitCellMedium];
}

@end
