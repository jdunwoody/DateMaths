//
//  DigitCell.h
//  DateMaths
//
//  Created by James Dunwoody on 8/06/2015.
//
//

#import <UIKit/UIKit.h>

@protocol DataItem;

typedef enum
{
    DigitCellSmall,
    DigitCellMedium,
} DigitCellCellSize;

IB_DESIGNABLE
@interface DigitCell : UIView

@property (weak, nonatomic) IBOutlet UILabel *label;

@property (weak, nonatomic) IBOutlet UILabel *starA;
@property (weak, nonatomic) IBOutlet UILabel *starB;
@property (weak, nonatomic) IBOutlet UILabel *starC;

@property (strong, nonatomic) IBInspectable NSString *text;
@property (strong, nonatomic) IBInspectable UIColor *colour;

- (void)configure:(DigitCellCellSize)cellSize;
- (void)unilluminate;

@end

