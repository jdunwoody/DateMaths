//
//  NonAnimatingSegue.h
//  DateMaths
//
//  Created by James Dunwoody on 19/07/2015.
//
//

#import <UIKit/UIKit.h>

@interface NonAnimatingSegue : UIStoryboardSegue

@end
