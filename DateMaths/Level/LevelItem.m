#import "LevelItem.h"
#import "DigitCollection.h"
#import "OperatorCollection.h"
#import "ResultsCollection.h"
#import "Digit.h"
#import "Operator.h"
#import "OperatorFactory.h"
#import "DigitFactory.h"
#import "Theme.h"
#import "Scoring.h"

@interface LevelItem ()
@property (nonatomic, strong) NSArray *cells;
@end

@implementation LevelItem

- (instancetype)initWithNumber:(NSInteger)number digitFactory:(DigitFactory *)digitFactory operatorFactory:(OperatorFactory *)operatorFactory
{
    self = [super init];
    if (!self) {
        return self;
    }

    _number = number;
    _starA = NO;
    _starB = NO;
    _starC = NO;

    _digitCollection = [[DigitCollection alloc] initWithDigits:digitFactory.digits];
    _operatorCollection = [[OperatorCollection alloc] initWithOperators:operatorFactory.operators];
    _resultsCollection = [[ResultsCollection alloc] init];

    self.cells = @[];

    return self;
}

- (id)copyWithZone:(NSZone *)zone
{
    return nil;
}

- (NSString *)value
{
    return [NSString stringWithFormat:@"%li", (long)self.number];
}

- (NSString *)displayValue
{
    return self.value;
}

- (void)updateStarsWithSum:(NSNumber *)sum witDigitsCollection:(DigitCollection *)digitCollection
{
    if (self.number == [sum doubleValue]) {
        for (NSUInteger i = 0; i < self.digitCollection.count; i++) {
            for (Digit *digit in self.digitCollection) {
                UIView *cell = self.cells[i];
                cell.backgroundColor = digit.used ? [Theme mainColour] : [Theme deselectedColour];
            }
        }
    }
}

- (void)useDigit:(Digit *)digit
{
    digit.used = YES;
    [self.resultsCollection addObject:digit];
}

- (void)useOperator:(Operator *)operator
{
    [self.resultsCollection addObject:operator];
}

- (BOOL)isDigit
{
    return NO;
}

- (BOOL)attempted
{
    return self.resultsCollection.count > 0;
}

- (NSUInteger)points
{
    Scoring *scoring = [[Scoring alloc] init];

    return [scoring calculate:self];
}

@end
