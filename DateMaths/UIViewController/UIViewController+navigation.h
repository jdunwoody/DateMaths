//
//  UIViewController+jamesGradientBackground.h
//  DateMaths
//
//  Created by James Dunwoody on 30/05/2015.
//
//

#import <UIKit/UIKit.h>

@interface UIViewController (navigation)

- (void)dateMaths_hideNavigationController;
- (void)dateMaths_showNavigationController;

@end
