//
//  LevelOverviewCollectionViewCell.m
//  DateMaths
//
//  Created by James Dunwoody on 28/06/2015.
//
//

#import "LevelOverviewCollectionViewCell.h"

@interface LevelOverviewCollectionViewCell ()
@property (nonatomic, readonly) CGFloat originPointSize;
@end

@implementation LevelOverviewCollectionViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];

    _originPointSize = self.label.font.pointSize;
}

- (void)configure
{
    self.label.textColor = [UIColor whiteColor];
}

- (void)configureAsSelected:(BOOL)selected
{
    CGFloat fontSize = selected ? self.originPointSize * 1.2f : self.originPointSize;
    UIColor *colour = selected ? [UIColor whiteColor] : [UIColor grayColor];
    self.label.font = [UIFont fontWithName:self.label.font.fontName size:fontSize];
    self.label.textColor = colour;
}

@end
