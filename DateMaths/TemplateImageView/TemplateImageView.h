//
//  TemplateImageView.h
//  NABConnect
//
//  Created by James Dunwoody on 11/06/2015.
//  Copyright (c) 2015 NAB. All rights reserved.
//

#import <UIKit/UIKit.h>


IB_DESIGNABLE
@interface TemplateImageView : UIImageView

@end
