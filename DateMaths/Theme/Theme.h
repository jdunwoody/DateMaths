//
//  Theme.h
//  DateMaths
//
//  Created by James Dunwoody on 30/05/2015.
//
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface Theme : NSObject

+ (UIFont *)fontWithSize:(int)fontSize;

+ (UIColor *)whiteColour;
+ (UIColor *)mainColour;
+ (UIColor *)backgroundGradientTopColour;
+ (UIColor *)backgroundGradientBottomColour;

+ (UIColor *)datePickerTextColor;
+ (UIColor *)deselectedColour;
+ (UIColor *)shadowColour;
+ (id)rainbowColours;
+ (CGFloat)lineWidth;
+ (UIColor *)digitColour;
+ (UIColor *)operatorColour;
+ (UIColor *)unusedCellColour;
@end
