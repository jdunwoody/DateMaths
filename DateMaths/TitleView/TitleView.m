//
// Created by James Dunwoody on 20/06/15.
//

#import "TitleView.h"
#import "UIView+DateMaths.h"


@implementation TitleView

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];

    if (self) {
        [self setupViewFromNib];

        [self configure];
    }

    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];

    if (self) {
        [self setupViewFromNib];

        [self configure];
    }

    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];

    [self configure];
}

- (void)configure
{
}
@end
