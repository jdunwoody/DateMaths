//
//  TweakerTableCellTableViewCell.h
//  DateMaths
//
//  Created by James Dunwoody on 16/06/2015.
//
//

#import <UIKit/UIKit.h>

@class TweakerOverlayViewController;
@class TweakableProperty;

@protocol TweakerStepperPressedDelegate<NSObject>
- (void)stepperPressed:(double)stepValue;
@end

@interface TweakerTableCellTableViewCell : UITableViewCell

- (IBAction)stepperPressed:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *propertyLabel;
@property (nonatomic, strong) TweakableProperty *tweakerProperty;
@property (weak, nonatomic) IBOutlet UILabel *valueLabel;
@end
