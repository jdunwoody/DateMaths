//
// Created by James Dunwoody on 3/04/15.
// Copyright (c) 2015 ___FULLUSERNAME___. All rights reserved.
//

#import "CALayer+DateMathsGlow.h"


@implementation CALayer (DateMathsGlow)

- (void)dateMaths_glowWithColor:(UIColor *)color
{
    self.shadowColor = [color CGColor];
    self.shadowRadius = 3.0f;
    self.shadowOpacity = 0.6f;
    self.shadowOffset = CGSizeMake(0.0f, 8.0f);

    self.masksToBounds = NO;
}

- (void)dateMaths_smallGlowWithColor:(UIColor *)color
{
    self.shadowColor = [color CGColor];
    self.shadowRadius = 2.5f;
    self.shadowOpacity = 0.6f;
    self.shadowOffset = CGSizeMake(0.0f, 6.0f);

    self.masksToBounds = NO;
}

- (void)dateMaths_borderWithColor:(UIColor *)color
{
    self.cornerRadius = 8.0f;//self.bounds.size.width / 40.0f;
    self.borderWidth = 4.0f;//self.bounds.size.width / 90.0f;
    self.borderColor = color.CGColor;

    self.masksToBounds = NO;
}

- (void)dateMaths_borderWithColor:(CGColorRef)color radius:(CGFloat)radius borderWidth:(CGFloat)borderWidth
{
    self.cornerRadius = radius;//8.0f;//self.bounds.size.width / 40.0f;
    self.borderWidth = borderWidth;//4.0f;//self.bounds.size.width / 90.0f;
    self.borderColor = color;//color.CGColor;

    self.masksToBounds = NO;
}

- (void)dateMaths_scaledBorderWithColor:(UIColor *)color
{
    self.cornerRadius = self.bounds.size.width / 40.0f;
    self.borderWidth = self.bounds.size.width / 90.0f;
    self.borderColor = color.CGColor;

    self.masksToBounds = NO;
}

- (void)dateMaths_smallBorderWithColor:(UIColor *)color
{
    self.cornerRadius = 6.0f;
    self.borderWidth = 3.0f;
    self.borderColor = color.CGColor;

    self.masksToBounds = NO;
}

@end
