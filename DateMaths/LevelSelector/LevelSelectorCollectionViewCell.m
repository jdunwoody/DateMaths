//
//  LevelSelectorCollectionViewCell.m
//  DateMaths
//
//  Created by James Dunwoody on 28/06/2015.
//
//

#import "LevelSelectorCollectionViewCell.h"
#import "LevelItem.h"
#import "DigitCell.h"
#import "DigitCollection.h"
#import "Digit.h"
#import "Theme.h"
#import "UIColor+NewCategory.h"

@interface LevelSelectorCollectionViewCell ()
@end

@implementation LevelSelectorCollectionViewCell

- (void)configureWithLevelItem:(LevelItem *)levelItem
{
    self.label.text = [NSString stringWithFormat:@"%li", (long)levelItem.number];
    self.pointsLabel.text = [NSString stringWithFormat:@"%u pts", (unsigned)levelItem.points];

    for (NSUInteger i = 0; i < levelItem.digitCollection.count; i++) {
        DigitCell *digitCell = self.cellViews[i];
        [digitCell configure:DigitCellSmall];

        Digit *digit = levelItem.digitCollection[i];

        if (digit.used) {
            digitCell.layer.borderColor = [Theme mainColour].CGColor;
        } else {
            digitCell.layer.borderColor = [UIColor dateMaths_lightGray].CGColor;
        }
    }
}

@end
