//
//  TweakerOverlayViewController.m
//  DateMaths
//
//  Created by James Dunwoody on 15/06/2015.
//
//

#import "TweakerOverlayViewController.h"
#import "TweakableProperty.h"

@interface TweakerOverlayViewController ()

@end

@implementation TweakerOverlayViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.properties = @[];
    [self.tableView registerNib:[UINib nibWithNibName:@"TweakerTableCellTableViewCell" bundle:nil] forCellReuseIdentifier:@"TweakerTableCellTableViewCell"];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.properties.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TweakerTableCellTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TweakerTableCellTableViewCell"];

    NSUInteger row = (NSUInteger)indexPath.row;
    TweakableProperty *property = self.properties[row];

    cell.tweakerProperty = property;
    cell.propertyLabel.text = property.label;

    return cell;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

@end
