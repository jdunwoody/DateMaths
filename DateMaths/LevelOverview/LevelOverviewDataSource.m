//
// Created by James Dunwoody on 28/06/15.
//

#import "LevelOverviewDataSource.h"
#import "LevelOverviewCollectionViewCell.h"
#import "LevelCollection.h"
#import "LevelItem.h"


@interface LevelOverviewDataSource ()
@property (nonatomic, readonly) NSArray *data;
@property (nonatomic, readonly) LevelCollection *levelCollection;
@end

@implementation LevelOverviewDataSource

- (instancetype)initWithLevelCollection:(LevelCollection *)levelCollection
{
    self = [super init];

    if (self) {
        _levelCollection = levelCollection;

        NSUInteger NUMBER_OF_LEVELS = 20;
        NSMutableArray *mutableData = [NSMutableArray arrayWithCapacity:NUMBER_OF_LEVELS];
        for (int i = 1; i <= 20; i++) {
            [mutableData addObject:@(i)];
        }
        _data = mutableData;
    }

    return self;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.data.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    LevelOverviewCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"levelOverviewCell" forIndexPath:indexPath];

    NSNumber *currentData = self.data[(NSUInteger)indexPath.row];

    cell.label.text = currentData.stringValue;
    BOOL isSelected = self.levelCollection.current.number - 1 == indexPath.row;
    [cell configureAsSelected:isSelected];

    return cell;
}

@end
