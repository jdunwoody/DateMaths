//
// Created by James Dunwoody on 27/05/15.
//

#import "UIColor+NewCategory.h"

@implementation UIColor (NewCategory)

+ (UIColor *)dateMaths_neonPurple
{
//    return [UIColor colorWithRed:252.0f / 255.0f green:79.0f / 255.0f blue:245.0f / 255.0f alpha:1.0f];
    return [UIColor colorWithRed:255.0f / 255.0f green:113.0f / 255.0f blue:247.0f / 255.0f alpha:1.0f];
}

+ (UIColor *)dateMaths_lightGray
{
    return [UIColor colorWithRed:255.0f / 255.0f green:255.0f / 255.0f blue:255.0f / 255.0f alpha:0.1f];
}

@end
