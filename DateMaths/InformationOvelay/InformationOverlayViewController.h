//
//  InformationOverlayViewController.h
//  DateMaths
//
//  Created by James Dunwoody on 13/07/2015.
//
//

#import <UIKit/UIKit.h>

@interface InformationOverlayViewController : UIViewController

- (void)showUpdatedScore:(NSUInteger)score animate:(BOOL)animations;
- (void)showInformation:(NSString *)string animated:(BOOL)animated;
@end
