//
//  NonAnimatingSegue.m
//  DateMaths
//
//  Created by James Dunwoody on 19/07/2015.
//
//

#import "NonAnimatingSegue.h"

@implementation NonAnimatingSegue

- (void)perform
{
    UIViewController *source = self.sourceViewController;
    if (source.navigationController) {
        [source.navigationController pushViewController:self.destinationViewController animated:YES];
    }
}

@end
