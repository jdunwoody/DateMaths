//
//  LevelOverviewViewController.h
//  DateMaths
//
//  Created by James Dunwoody on 28/06/2015.
//
//

#import <UIKit/UIKit.h>

@class LevelOverviewDataSource;
@class DateMathsViewController;

@protocol LevelOverviewViewControllerDelegate
- (void)levelOverviewTapped;
@end

@interface LevelOverviewViewController : UIViewController

@property (nonatomic) LevelOverviewDataSource *dataSource;
@property (strong, nonatomic) IBOutlet UITapGestureRecognizer *tapGestureRecogniser;
@property (nonatomic) id<LevelOverviewViewControllerDelegate> delegate;

- (IBAction)tappedView:(id)sender;
- (void)reloadData;

@end
