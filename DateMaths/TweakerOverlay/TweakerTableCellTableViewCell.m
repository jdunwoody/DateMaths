//
//  TweakerTableCellTableViewCell.m
//  DateMaths
//
//  Created by James Dunwoody on 16/06/2015.
//
//

#import "TweakerTableCellTableViewCell.h"
#import "TweakableProperty.h"
#import "IntroViewController.h"

@implementation TweakerTableCellTableViewCell

- (IBAction)stepperPressed:(id)sender
{
    UIStepper *stepper = sender;
    double stepperValue = stepper.value;

    self.valueLabel.text = [NSString stringWithFormat:@"%.2f", stepperValue];
    [self.tweakerProperty.stepperValueChangedTarget performSelector:self.tweakerProperty.stepperValueChangedSelector withObject:@(stepperValue)];
}

@end
