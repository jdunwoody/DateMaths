#import "LevelCollectionViewDataSource.h"
#import "LevelCollection.h"
#import "SimpleCollectionViewCell.h"
#import "DataItem.h"
#import "LevelItem.h"
#import "DigitCell.h"
#import "Theme.h"

@interface LevelCollectionViewDataSource ()
@property (nonatomic, strong) LevelCollection *levelCollection;
@end

@implementation LevelCollectionViewDataSource

- (instancetype)initWithCollection:(LevelCollection *)levelCollection
{
    self = [super init];
    if (!self) {
        return self;
    }

    _levelCollection = levelCollection;

    return self;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.levelCollection count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    SimpleCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"simpleCell" forIndexPath:indexPath];
    LevelItem *item = self.levelCollection[(NSUInteger)indexPath.row];
    cell.digitCell.text = item.displayValue;
//    cell.digitCell.illuminated = self.levelCollection.current == item;

    cell.digitCell.starA.hidden = !item.starA;
    cell.digitCell.starB.hidden = !item.starB;
    cell.digitCell.starC.hidden = !item.starC;

    cell.digitCell.colour = [Theme mainColour];
    [cell configure];

    return cell;
}

@end
