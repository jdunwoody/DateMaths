//
//  IntroViewController.h
//  DateMaths
//
//  Created by James Dunwoody on 5/05/2015.
//
//

#import <UIKit/UIKit.h>

@class DigitCell;
@class TweakerOverlayViewController;
@class TitleView;

@interface IntroViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *animateButton;


@property (weak, nonatomic) IBOutlet TitleView *titleView;
- (IBAction)animateButtonTapped:(id)sender;
@end
