//
//  UIViewController+jamesGradientBackground.m
//  DateMaths
//
//  Created by James Dunwoody on 30/05/2015.
//
//

#import "UIViewController+navigation.h"
#import "Theme.h"

@implementation UIViewController (navigation)
- (void)dateMaths_hideNavigationController
{
    self.navigationController.navigationBarHidden = YES;
}

- (void)dateMaths_showNavigationController
{
    self.navigationController.navigationBarHidden = NO;
}

@end
