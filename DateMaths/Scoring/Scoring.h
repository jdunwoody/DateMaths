//
//  Scoring.h
//  DateMaths
//
//  Created by James Dunwoody on 5/07/2015.
//
//

#import <Foundation/Foundation.h>

@class LevelItem;

@interface Scoring : NSObject

- (NSUInteger)calculate:(LevelItem *)levelItem;
@end
