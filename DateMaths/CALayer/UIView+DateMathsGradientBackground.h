//
// Created by James Dunwoody on 6/06/15.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface UIView (DateMathsGradientBackground)

- (void)dateMaths_gradientRainbowBackground;
- (void)dateMaths_gradientBackgroundWhiteToGray;
- (void)dateMaths_gradientFromColour:(UIColor *)color toColour:(UIColor *)colour;

@end
