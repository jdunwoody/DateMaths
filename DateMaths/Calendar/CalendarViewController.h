//
//  CalendarViewController.h
//  DateMaths
//
//  Created by James Dunwoody on 26/05/2015.
//
//

#import <UIKit/UIKit.h>
#import "CollectionDataSourceDelegate.h"

@class CalendarDataSource;
@class DigitCollectionDataSource;

@interface CalendarViewController : UIViewController<UIPickerViewDelegate, CollectionDataSourceDelegate, UICollectionViewDelegate>

@property (strong, nonatomic) UIColor *datePickerTextColor;
@property (weak, nonatomic) IBOutlet UIPickerView *picker;
@property (weak, nonatomic) IBOutlet UIButton *next;

@property (weak, nonatomic) IBOutlet UIView *frontCard;
@property (weak, nonatomic) IBOutlet UIView *backCard;
@property (weak, nonatomic) IBOutlet UIView *flipContainerView;
@property (weak, nonatomic) IBOutlet UICollectionView *digitCollection;
- (IBAction)nextButtonTapped:(id)sender;
@end
