#import <UIKit/UIKit.h>

@protocol DataItem;
@class DigitCell;

static CGFloat SIMPLE_COLLECTION_VIEW_CELL_WIDTH = 60.0f;
static CGFloat SIMPLE_COLLECTION_VIEW_CELL_HEIGHT = 60.0f;
static CGFloat SIMPLE_COLLECTION_VIEW_CELL_HORIZONTAL_SPACING = 0.0f;
static CGFloat SIMPLE_COLLECTION_VIEW_CELL_VERTICAL_SPACING = 0.0f;

@interface SimpleCollectionViewCell : UICollectionViewCell

@property (nonatomic, weak) IBOutlet DigitCell *digitCell;

- (void)configure;

@end

