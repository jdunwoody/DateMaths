//
//  Theme.m
//  DateMaths
//
//  Created by James Dunwoody on 30/05/2015.
//
//

#import "Theme.h"
#import "UIColor+NewCategory.h"

@implementation Theme

+ (UIColor *)mainColour
{
    return [UIColor dateMaths_neonPurple];
}

+ (UIColor *)whiteColour
{
    return [UIColor whiteColor];
}

+ (UIFont *)fontWithSize:(int)fontSize
{
    return [UIFont fontWithName:@"NEON GLOW" size:fontSize];
}

+ (UIColor *)backgroundGradientTopColour
{
    return [UIColor colorWithRed:(CGFloat)(42.0 / 255.0) green:(CGFloat)(93.0 / 255.0) blue:(CGFloat)(124.0 / 255.0) alpha:1.0];
}

+ (UIColor *)backgroundGradientBottomColour
{
    return [UIColor colorWithRed:(CGFloat)(45.0 / 255.0) green:(CGFloat)(74.0 / 255.0) blue:(CGFloat)(93.0 / 255.0) alpha:1.0];
}

+ (UIColor *)datePickerTextColor
{
    return [UIColor dateMaths_neonPurple];
}

+ (UIColor *)deselectedColour
{
    return [UIColor blackColor];
}

+ (UIColor *)shadowColour
{
    return [UIColor colorWithRed:(CGFloat)(0.0f / 255.0) green:(CGFloat)(0.0f / 255.0) blue:(CGFloat)(0.0f / 255.0) alpha:0.61f];
}

+ (id)rainbowColours
{
    return @[[UIColor purpleColor], [UIColor redColor], [UIColor orangeColor], [UIColor cyanColor], [UIColor blueColor], [UIColor greenColor]];
}

+ (CGFloat)lineWidth
{
    return 4.0f;
}

+ (UIColor *)digitColour
{
    return [UIColor dateMaths_neonPurple];
}

+ (UIColor *)operatorColour
{
    return [UIColor whiteColor];
}

+ (UIColor *)unusedCellColour
{
    return [UIColor colorWithRed:(CGFloat)(0.0f / 255.0) green:(CGFloat)(0.0f / 255.0) blue:(CGFloat)(0.0f / 255.0) alpha:0.61f];
}

@end
