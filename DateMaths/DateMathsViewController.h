//
//  DateMathsViewController.h
//  DateMaths
//
//  Created by James Dunwoody on 29/03/2015.
//  Copyright (c) 2015 James Dunwoody. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CollectionDataSourceDelegate.h"
#import "LevelOverviewViewController.h"
#import "LevelSelectorViewController.h"

@class ResultsCollectionViewDataSource;
@class OperatorCollectionDataSource;
@class ResultsCollection;
@class LevelCollection;
@class ResultCollectionViewLayout;
@class Sounds;

@protocol CollectionDataSourceDelegate;
@class LevelOverviewViewController;
@class LevelSelectorViewController;
@class LevelSelectorDataSource;
@class InformationOverlayViewController;

@interface DateMathsViewController : UIViewController<UICollectionViewDelegate, CollectionDataSourceDelegate, LevelOverviewViewControllerDelegate, LevelSelectorViewControllerDelegate>

@property (weak, nonatomic) IBOutlet UICollectionView *digitCollectionView;
@property (weak, nonatomic) IBOutlet UICollectionView *operatorCollectionView;
@property (weak, nonatomic) IBOutlet UICollectionView *resultsCollectionView;
@property (weak, nonatomic) IBOutlet UIView *levelOverviewContainer;
@property (weak, nonatomic) IBOutlet UIView *backgroundView;
@property (weak, nonatomic) IBOutlet UIView *scoreContainer;

@property (weak, nonatomic) IBOutlet UILabel *totalLabel;
@property (strong, nonatomic) IBOutlet UIPanGestureRecognizer *dragResultsPanGestureRecogniser;
- (IBAction)selectedStarButtonItem:(id)sender;
- (IBAction)selectedCalendarButtonItem:(id)sender;

@property (nonatomic, strong) NSDate *date;
- (IBAction)panned:(id)sender;
- (IBAction)backNavigationButtonTapped:(id)sender;

@end
