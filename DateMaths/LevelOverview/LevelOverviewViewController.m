//
//  LevelOverviewViewController.m
//  DateMaths
//
//  Created by James Dunwoody on 28/06/2015.
//
//

#import "LevelOverviewViewController.h"
#import "LevelOverviewDataSource.h"
#import "DateMathsViewController.h"

@interface LevelOverviewViewController ()<UICollectionViewDelegate>

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@end

@implementation LevelOverviewViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.collectionView.dataSource = self.dataSource;
    self.collectionView.delegate = self;

    UINib *nib = [UINib nibWithNibName:@"LevelOverviewCollectionViewCell" bundle:[NSBundle mainBundle]];
    [self.collectionView registerNib:nib forCellWithReuseIdentifier:@"levelOverviewCell"];
}

- (IBAction)tappedView:(id)sender
{
    [self.delegate levelOverviewTapped];
}

- (void)reloadData
{
    [self.collectionView reloadData];
}

@end
