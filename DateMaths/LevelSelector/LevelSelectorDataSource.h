//
// Created by James Dunwoody on 28/06/15.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class LevelCollection;

@interface LevelSelectorDataSource : NSObject<UICollectionViewDataSource>

- (instancetype)initWithLevelCollection:(LevelCollection *)collection;

@end
