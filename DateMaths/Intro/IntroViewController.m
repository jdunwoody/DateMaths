//
//  IntroViewController.m
//  DateMaths
//
//  Created by James Dunwoody on 5/05/2015.
//
//

#import "IntroViewController.h"
#import "UIViewController+navigation.h"
#import "DigitCell.h"
#import "Theme.h"
#import "TweakerOverlayViewController.h"
#import "TweakableProperty.h"
#import "TitleView.h"

@interface IntroViewController ()
@property (nonatomic) NSArray *cells;
@property (nonatomic, strong) NSTimer *timer;
@property (nonatomic, readonly) TweakerOverlayViewController *tweakerOverlay;
@property (nonatomic) NSNumber *firstPulseDuration;
@property (nonatomic) NSNumber *initialDelay;
@property (nonatomic) NSNumber *firstDimDuration;
@end

@implementation IntroViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.cells = @[self.titleView.dateDCell, self.titleView.dateACell, self.titleView.dateTCell, self.titleView.dateECell, self.titleView.mathsMCell, self.titleView.mathsACell, self.titleView.mathsTCell, self.titleView.mathsHCell, self.titleView.mathsSCell];

    [self unilluminateAllCells];

    self.timer = [NSTimer scheduledTimerWithTimeInterval:2.0 target:self selector:@selector(timerFireMethod:) userInfo:nil repeats:NO];

    [self dateMaths_hideNavigationController];

    self.initialDelay = @1.5f;
    self.firstPulseDuration = @0.2f;
    self.firstDimDuration = @0.2f;
}

- (void)addTweakerView
{
    _tweakerOverlay = [[TweakerOverlayViewController alloc] initWithNibName:@"TweakerOverlay" bundle:nil];
    [self addChildViewController:self.tweakerOverlay];
    CGRect tweakerFrame = CGRectMake(0.0f, self.view.bounds.size.height * 2.0f / 3.0f, self.view.bounds.size.width, self.view.bounds.size.height / 3.0f);
    self.tweakerOverlay.view.frame = tweakerFrame;
    [self.view addSubview:self.tweakerOverlay.view];
    [self.tweakerOverlay didMoveToParentViewController:self];

    self.tweakerOverlay.properties = @[
        [[TweakableProperty alloc] initWithLabel:@"Initial delay" targetSelector:(@selector(setInitialDelay:)) target:self],
        [[TweakableProperty alloc] initWithLabel:@"First pulse duration" targetSelector:(@selector(setFirstPulseDuration:)) target:self],
        [[TweakableProperty alloc] initWithLabel:@"First dim duration" targetSelector:(@selector(setFirstDimDuration:)) target:self],
    ];

    self.animateButton.hidden = NO;
}

- (void)setInitialDelay:(NSNumber *)initialDelay
{
    _initialDelay = initialDelay;
}

- (void)setFirstDimDuration:(NSNumber *)firstDimDuration
{
    _firstDimDuration = firstDimDuration;
}

- (void)setFirstPulseDuration:(NSNumber *)firstPulseDuration
{
    _firstPulseDuration = firstPulseDuration;
}

- (void)unilluminateAllCells
{
    for (DigitCell *cell in self.cells) {
        [cell unilluminate];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    ((CALayer *)self.view.layer.sublayers[0]).frame = self.view.bounds;

    self.navigationController.navigationBar.hidden = YES;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];

    [self configureAnimatedLightingForCell];
}

- (void)timerFireMethod:(NSTimer *)timer
{
    [self performSegueWithIdentifier:@"datePicker" sender:self];
}

- (void)configureAnimatedLightingForCell;
{
    for (DigitCell *cell in self.cells) {
        [self flickeringAnimation:cell initialDelay:[self randomFloat:self.initialDelay]];
    }
}

- (float)randomFloat:(NSNumber *)number
{
    return arc4random_uniform((number.floatValue * 100.0f)) / 100.0f;
}

- (void)flickeringAnimation:(DigitCell *)cell initialDelay:(float)initialDelay
{
    [UIView transitionWithView:cell.label duration:initialDelay options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
        [cell unilluminate];

    } completion:^(BOOL finished) {
        [UIView transitionWithView:cell.label duration:self.firstPulseDuration.doubleValue options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
            cell.layer.borderColor = cell.colour.CGColor;

        } completion:^(BOOL finished2) {
            [UIView transitionWithView:cell.label duration:self.firstDimDuration.doubleValue options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
                cell.layer.borderColor = [Theme deselectedColour].CGColor;

            } completion:^(BOOL finished3) {
                cell.label.textColor = cell.colour;
                cell.layer.borderColor = cell.colour.CGColor;
            }];
        }];
    }];
}

- (IBAction)animateButtonTapped:(id)sender
{
    [self unilluminateAllCells];

    [self configureAnimatedLightingForCell];
}

@end
