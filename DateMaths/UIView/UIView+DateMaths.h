//
//  UIView+DateMaths.h
//  DateMaths
//
//  Created by James Dunwoody on 13/06/2015.
//
//

#import <UIKit/UIKit.h>

@interface UIView (DateMaths)

/**
*  Load this views matching NIB and load it into this view.
*  Only call from initWithFrame: and initWithCoder:
*/
- (void)setupViewFromNib;
- (void)setupViewFromNibWithView:(UIView *)view;

/**
*  Load this views NIB
*
*  @return a UIView loaded from the NIB with this instance as owner.
*/
- (UIView *)viewFromNib;

/**
*  Name of the NIB used by loadNib and setupNib.
*
*  @see loadNib setupNib
*  @return name of class or overwritten name
*/
- (NSString *)nibName;

@end
