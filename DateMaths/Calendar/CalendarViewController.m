//
//  CalendarViewController.m
//  DateMaths
//
//  Created by James Dunwoody on 26/05/2015.
//
//

#import "CalendarViewController.h"
#import "CalendarDataSource.h"
#import "UIViewController+navigation.h"
#import "Theme.h"
#import "DateMathsViewController.h"
#import "DigitFactory.h"
#import "OperatorFactory.h"
#import "DigitCollectionDataSource.h"
#import "LevelCollection.h"

@interface CalendarViewController ()

@property (nonatomic, readonly) CalendarDataSource *dataSource;
@property (nonatomic) BOOL flipped;
@property (nonatomic) DigitCollectionDataSource *digitCollectionDataSource;
@end

@implementation CalendarViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    _dataSource = [[CalendarDataSource alloc] init];
    self.picker.dataSource = self.dataSource;
    [self.next setTitleColor:[Theme mainColour] forState:UIControlStateNormal];
    [self dateMaths_hideNavigationController];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    self.picker.hidden = NO;
    self.digitCollection.hidden = YES;
    self.next.hidden = NO;

    ((CALayer *)self.view.layer.sublayers[0]).frame = self.view.bounds;
    self.navigationController.navigationBar.hidden = YES;

    [self initialiseDigitCollectionPosition];
}

- (void)initialiseDigitCollectionPosition
{
    self.digitCollection.hidden = YES;
    self.digitCollection.frame = CGRectMake(self.picker.frame.origin.x, self.picker.frame.origin.y, self.picker.frame.size.width, 122.0f);
    [self.digitCollection layoutIfNeeded];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];

    [self animateDatePickerToDate:[NSDate date]];
}

- (void)animateDatePickerToDate:(NSDate *)date
{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:date];

    [self.picker selectRow:components.day - 1 inComponent:0 animated:YES];
    [self.picker selectRow:components.month - 1 inComponent:1 animated:YES];
    [self.picker selectRow:components.year - 1970 inComponent:2 animated:YES];
}

- (void)animateFlip
{
    [UIView transitionWithView:self.flipContainerView
        duration:1
        options:UIViewAnimationOptionTransitionFlipFromLeft | UIViewAnimationOptionRepeat
        animations:^{

            if (!self.flipped) {
                self.frontCard.hidden = YES;
                self.backCard.hidden = NO;
                self.flipped = YES;
            } else {
                self.frontCard.hidden = NO;
                self.backCard.hidden = YES;
                self.flipped = NO;
            }

        } completion:nil];
}

- (NSAttributedString *)pickerView:(UIPickerView *)pickerView attributedTitleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    NSString *title = [self pickerView:pickerView titleForRow:row forComponent:component];

    NSAttributedString *attString = [[NSAttributedString alloc] initWithString:title attributes:@{NSForegroundColorAttributeName : [Theme datePickerTextColor]}];
    return attString;
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    if (!view) {
        view = [[UILabel alloc] initWithFrame:CGRectZero];
    }

    NSString *title = [self pickerView:pickerView titleForRow:row forComponent:component];

    UILabel *label = (UILabel *)view;
    label.text = title;
    label.textColor = [Theme mainColour];

    UIFont *font = [Theme fontWithSize:32];
    label.font = font;

    return view;
}

- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component
{
    return 80.0f;
}

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
{
    return 80.0f;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    NSUInteger titleInteger;

    NSString *value;

    switch ((ComponentsInDatePicker)component) {
        case CalendarDatePickerDayComponent:
            titleInteger = [self.dataSource dayValueAtRow:row];
            value = [NSString stringWithFormat:@"%02lu", (unsigned long)titleInteger];
            break;
        case CalendarDatePickerMonthComponent:
            titleInteger = [self.dataSource monthValueAtRow:row];
            value = [NSString stringWithFormat:@"%02lu", (unsigned long)titleInteger];
            break;
        case CalendarDatePickerYearComponent:
            titleInteger = [self.dataSource yearValueAtRow:row];
            value = [NSString stringWithFormat:@"%04lu", (unsigned long)titleInteger];
            break;
    }

    return value;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
}

- (IBAction)nextButtonTapped:(id)sender
{
    [self animateCalendarToDigitLocation];
}

- (void)animateCalendarToDigitLocation
{
    self.digitCollection.hidden = NO;
    self.digitCollection.alpha = 0.0f;
    self.next.hidden = YES;

    [UIView transitionWithView:self.picker duration:0.8f options:UIViewAnimationOptionCurveEaseInOut | UIViewAnimationOptionAllowAnimatedContent animations:^{
        self.digitCollection.alpha = 1.0f;
        NSDate *date = [self selectedDate];
        [self showDigitsForDate:date];

    } completion:^(BOOL finished) {
        [UIView transitionWithView:self.digitCollection duration:1.05f options:UIViewAnimationOptionCurveEaseInOut | UIViewAnimationOptionAllowAnimatedContent animations:^{
            CGFloat height = self.view.frame.size.height / 4.0f;
            CGFloat width = self.view.frame.size.width * 2.0f / 3.0f;

            self.digitCollection.frame = CGRectMake(0.0f, self.view.frame.size.height - height, width, height);

        } completion:^(BOOL finished2) {
            [self performSegueWithIdentifier:@"play" sender:self];
        }];
    }];
}

- (void)showDigitsForDate:(NSDate *)date
{
    DigitFactory *digitFactory = [[DigitFactory alloc] initWithDate:date];
    OperatorFactory *operatorFactory = [[OperatorFactory alloc] initWithSymbols:@[]];
    LevelCollection *levelCollection = [[LevelCollection alloc] initWithDigitFactory:digitFactory operatorFactory:operatorFactory];

    //Digits
    self.digitCollectionDataSource = [[DigitCollectionDataSource alloc] initWithLevelCollection:levelCollection withDelegate:self];
    self.digitCollection.dataSource = self.digitCollectionDataSource;
    self.digitCollection.delegate = self;
    UINib *nib = [UINib nibWithNibName:@"SimpleCollectionViewCell" bundle:[NSBundle mainBundle]];
    [self.digitCollection registerNib:nib forCellWithReuseIdentifier:@"simpleCell"];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"play"]) {
        DateMathsViewController *dateMathsViewController = segue.destinationViewController;

        NSDate *date = [self selectedDate];

        dateMathsViewController.date = date;
    }
}

- (NSDate *)selectedDate
{
    NSDateComponents *comps = [[NSDateComponents alloc] init];
    NSInteger year = [self.dataSource yearValueAtRow:[self.picker selectedRowInComponent:2]];
    [comps setYear:year];
    NSInteger month = [self.dataSource monthValueAtRow:[self.picker selectedRowInComponent:1]];
    [comps setMonth:month];
    NSInteger day = [self.dataSource dayValueAtRow:[self.picker selectedRowInComponent:0]];

    [comps setDay:day];
    NSDate *date = [[NSCalendar currentCalendar] dateFromComponents:comps];
    return date;
}

- (void)didLayoutCell:(NSIndexPath *)path inCollectionView:(UICollectionView *)view
{

}


@end
