//
//  GradientBackground.m
//  DateMaths
//
//  Created by James Dunwoody on 14/06/2015.
//
//

#import "GradientBackground.h"
#import "UIView+DateMathsGradientBackground.h"

@implementation GradientBackground

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];

    if (self) {
        [self configure];
    }

    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];

    if (self) {
        [self configure];
    }

    return self;
}

- (void)prepareForInterfaceBuilder
{
    [super prepareForInterfaceBuilder];
    [self configure];
}

- (void)configure
{
    [self dateMaths_gradientFromColour:self.fromColour toColour:self.toColour];
}

@end
