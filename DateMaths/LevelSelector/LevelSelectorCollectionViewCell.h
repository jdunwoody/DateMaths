//
//  LevelSelectorCollectionViewCell.h
//  DateMaths
//
//  Created by James Dunwoody on 28/06/2015.
//
//

#import <UIKit/UIKit.h>

@class LevelItem;
@class TemplateImageView;
@class DigitCell;

IB_DESIGNABLE
@interface LevelSelectorCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UILabel *label;
@property (weak, nonatomic) IBOutlet UILabel *pointsLabel;
@property (strong, nonatomic) IBOutletCollection(DigitCell) NSArray *cellViews;

- (void)configureWithLevelItem:(LevelItem *)levelItem;
@end
